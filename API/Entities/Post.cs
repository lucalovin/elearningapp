using System.Collections.Generic;

namespace API.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public ICollection<Document> Documents { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public Subject Subject { get; set; }
        public int SubjectId { get; set; }
    }
}