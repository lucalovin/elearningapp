namespace API.Entities
{
    public class Mark
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public Subject Subject { get; set; }
        public int SubjectId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}