using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs;
using API.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    public class CoursesController : BaseApiController
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IMapper _mapper;

        public CoursesController(ICourseRepository courseRepository, IMapper mapper)
        {
            _courseRepository = courseRepository;
            _mapper = mapper;
        }

        // api/users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubjectDto>>> GetCourses()
        {
            return Ok(await _courseRepository.GetCoursesAsync());
        }

        // api/users/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<SubjectDto>> GetCourse(int id)
        {
            return await _courseRepository.GetCourseAsync(id);
        }
    }
}