using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using API.DTOs;
using API.Entities;
using API.Extensions;
using API.Helpers;
using API.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    public class PostsController : BaseApiController
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        public IFileService _fileService;

        public PostsController(IPostRepository postRepository, IMapper mapper, IFileService fileService)
        {
            _fileService = fileService;
            _postRepository = postRepository;
            _mapper = mapper;
        }

        // api/posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostDto>>> GetPosts([FromQuery]PaginationParams paginationParams)
        {
            var posts = await _postRepository.GetPostsAsync(paginationParams);

            Response.AddPaginationHeader(posts.CurrentPage, posts.PageSize, posts.TotalCount, posts.TotalPages);

            return Ok(posts);
        }

        // api/posts/{id}
        [HttpGet("{id}", Name = "GetPost")]
        public async Task<ActionResult<PostDto>> GetPost(int id)
        {
            return await _postRepository.GetPostAsync(id);
        }

 
        [Authorize(Policy = "RequireProfessorRole")]
        [HttpPut]
        public async Task<ActionResult> UpdatePost(PostUpdateDto postUpdateDto)
        {
            var id = postUpdateDto.Id;
            var post = await _postRepository.GetPostByIdAsync(id);

            _mapper.Map(postUpdateDto, post);

            _postRepository.Update(post);

            if (await _postRepository.SaveAllAsync())
            {
                return NoContent();
            }

            return BadRequest("Failed to update user");
        }

        [Authorize(Policy = "RequireProfessorRole")]
        [HttpPost("{id}/add-file")]
        public async Task<ActionResult<DocumentDto>> AddFile(int id, IFormFile file)
        {
            var post = await _postRepository.GetPostByIdAsync(id);

            var result = await _fileService.AddFileAsync(file);

            if (result.Error != null) {
                return BadRequest(result.Error.Message);
            }

            var document = new Document
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId,
                Name = file.FileName
            };

            post.Documents.Add(document);

            if (await _postRepository.SaveAllAsync()) {
                return CreatedAtRoute("GetPost", new { id = post.Id }, _mapper.Map<DocumentDto>(document));
            }

            return BadRequest("Problem adding photo");
        }

        [Authorize(Policy = "RequireProfessorRole")]
        [HttpDelete("{id}/delete-file/{fileId}")]
        public async Task<ActionResult> DeleteFile(int id, int fileId)
        {
            var post = await _postRepository.GetPostByIdAsync(id);

            var file = post.Documents.FirstOrDefault(x => x.Id == fileId);

            if (file == null) {
                return NotFound();
            }

            if (file.PublicId != null) {
                var result = await _fileService.DeleteFileAsync(file.PublicId);
                if (result.Error != null) {
                    return BadRequest(result.Error.Message);
                }
            }

            post.Documents.Remove(file);

            if (await _postRepository.SaveAllAsync()) {
                return Ok();
            }

            return BadRequest("Failed to delete file");
        }
    }
}