using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.DTOs;
using API.Entities;
using API.Helpers;
using API.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class PostRepository : IPostRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public PostRepository(DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<PostDto> GetPostAsync(int id)
        {
            return await _context.Posts
                .Where(x => x.Id == id)
                .ProjectTo<PostDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async Task<Post> GetPostByIdAsync(int id)
        {
            return await _context.Posts
                .Include(p => p.Documents)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<PagedList<PostDto>> GetPostsAsync(PaginationParams paginationParams)
        {
            var query = _context.Posts
                .ProjectTo<PostDto>(_mapper.ConfigurationProvider)
                .AsNoTracking()
                .AsQueryable();
            
            if (paginationParams.SubjectName != null && paginationParams.SubjectName != string.Empty) {
                query = query.Where(p => p.Subject.Name == paginationParams.SubjectName);
            }

            return await PagedList<PostDto>.CreateAsync(query, paginationParams.PageNumber, paginationParams.PageSize);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Post post)
        {
            _context.Entry(post).State = EntityState.Modified;
        }
    }
}