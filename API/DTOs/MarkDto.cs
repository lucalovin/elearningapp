namespace API.DTOs
{
    public class MarkDto
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string SubjectName { get; set; }
    }
}