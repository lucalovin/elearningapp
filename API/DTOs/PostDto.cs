using System.Collections.Generic;

namespace API.DTOs
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public ICollection<DocumentDto> Documents { get; set; }
        public MemberDetailsDto User { get; set; }
        public SubjectDetailsDto Subject { get; set; }
    }
}