using System.Collections.Generic;

namespace API.DTOs
{
    public class PostUpdateDto
    {
        public int Id { get; set; } 
        public string Title { get; set; }
        public string Message { get; set; }
    }
}