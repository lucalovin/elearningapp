namespace API.DTOs
{
    public class SubjectDetailsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}