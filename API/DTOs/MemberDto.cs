using System.Collections.Generic;

namespace API.DTOs
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public ICollection<SubjectDto> Subjects { get; set; }
        public ICollection<PostDto> Posts { get; set; }
        public ICollection<MarkDto> Marks { get; set; }
    }
}