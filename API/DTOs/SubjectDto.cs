using System.Collections.Generic;
using API.Entities;

namespace API.DTOs
{
    public class SubjectDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<MemberDto> Users { get; set; }
        public ICollection<PostDto> Posts { get; set; }
    }
}