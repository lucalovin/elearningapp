namespace API.DTOs
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}