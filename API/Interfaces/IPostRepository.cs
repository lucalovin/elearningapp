using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs;
using API.Entities;
using API.Helpers;

namespace API.Interfaces
{
    public interface IPostRepository
    {
        void Update(Post post);
        Task<bool> SaveAllAsync();
        Task<Post> GetPostByIdAsync(int id);
        Task<PagedList<PostDto>> GetPostsAsync(PaginationParams userParams);
        Task<PostDto> GetPostAsync(int id);
    }
}