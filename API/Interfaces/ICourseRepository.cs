using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs;
using API.Entities;

namespace API.Interfaces
{
    public interface ICourseRepository
    {
        void Update(Subject user);
        Task<bool> SaveAllAsync();
        Task<IEnumerable<SubjectDto>> GetCoursesAsync();
        Task<SubjectDto> GetCourseAsync(int id);
    }
}