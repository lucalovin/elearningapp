using System.Linq;
using API.DTOs;
using API.Entities;
using API.Extensions;
using AutoMapper;

namespace API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, MemberDto>();
            CreateMap<Post, PostDto>();
            CreateMap<PostUpdateDto, Post>();
            CreateMap<User, MemberDetailsDto>();
            CreateMap<Subject, SubjectDto>();
            CreateMap<Subject, SubjectDetailsDto>();
            CreateMap<Mark, MarkDto>()
                .ForMember(dest => dest.SubjectName, opt => opt.MapFrom(src => src.Subject.Name));
            CreateMap<Document, DocumentDto>();
            CreateMap<Message, MessageDto>()
                .ForMember(dest => dest.RecipientName, opt => opt.MapFrom(src => src.Recipient.Name))
                .ForMember(dest => dest.SenderName, opt => opt.MapFrom(src => src.Sender.Name));
            CreateMap<RegisterDto, User>();
        }
    }
}