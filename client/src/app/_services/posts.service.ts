import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PaginatedResult } from '../_models/pagination';
import { PaginationParams } from '../_models/paginationParams';
import { Post } from '../_models/post';
import { getPaginatedResult, getPaginationHeaders } from './paginationHelper';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  baseUrl = environment.apiUrl;
  posts: Post[] = [];

  constructor(private http: HttpClient) { }

  getPosts(paginationParams: PaginationParams) {
    let params = getPaginationHeaders(paginationParams.pageNumber, paginationParams.pageSize);

    params = params.append('subjectName', paginationParams.subjectName);

    return getPaginatedResult<Post[]>(this.baseUrl + 'posts', params, this.http);
  }



  getPost(id: number) {
    const post = this.posts.find(x => x.id === id);
    if (post !== undefined) {
      return of(post);
    }
    return this.http.get<Post>(this.baseUrl + 'posts/' + id);
  }

  updatePost(post: Post) {
    return this.http.put(this.baseUrl + 'posts', post).pipe(
      map(() => {
        const index = this.posts.indexOf(post);
        this.posts[index] = post;
      })
    )
  }

  deleteFile(postId: number, fileId: number) {
    return this.http.delete(this.baseUrl + 'posts/' + postId + '/delete-file/' + fileId);
  }
}
