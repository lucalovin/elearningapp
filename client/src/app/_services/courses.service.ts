import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Member } from '../_models/member';
import { Post } from '../_models/post';
import { Subject } from '../_models/subject';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  baseUrl = environment.apiUrl;
  courses: Subject[] = [];

  constructor(private http: HttpClient) { }

  getCourses(): Observable<Subject[]> {
    if (this.courses.length > 0) {
      return of(this.courses);
    }
    return this.http.get<Subject[]>(this.baseUrl + 'courses').pipe(
      map(courses => {
        this.courses = courses;
        return courses;
      })
    );
  }

  getCourse(id: number) {
    const course = this.courses.find(x => x.id === id);
    if (course !== undefined) {
      return of(course);
    }
    return this.http.get<Subject>(this.baseUrl + 'courses/' + id);
  }
}


