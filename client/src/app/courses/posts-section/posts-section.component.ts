import { Component, Input, OnInit } from '@angular/core';
import { Member } from 'src/app/_models/member';
import { Post } from 'src/app/_models/post';

@Component({
  selector: 'app-posts-section',
  templateUrl: './posts-section.component.html',
  styleUrls: ['./posts-section.component.css']
})
export class PostsSectionComponent implements OnInit {
  @Input() member : Member;
  @Input() post: Post;

  constructor() { }

  ngOnInit(): void {
  }
}
