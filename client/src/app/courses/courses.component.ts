import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from '../_models/subject';
import { CoursesService } from '../_services/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses$: Observable<Subject[]>;

  constructor(private coursesService: CoursesService) { }

  ngOnInit(): void {
    this.courses$ = this.coursesService.getCourses();
  }
}
