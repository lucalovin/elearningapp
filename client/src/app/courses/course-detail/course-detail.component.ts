import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'src/app/_models/subject';
import { CoursesService } from 'src/app/_services/courses.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
  subject: Subject;

  constructor(private courseService: CoursesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadSubject();
  }

  loadSubject() {
    this.courseService.getCourse(+this.route.snapshot.paramMap.get('id')).subscribe(subject => {
      this.subject = subject;
    })
  }
}
