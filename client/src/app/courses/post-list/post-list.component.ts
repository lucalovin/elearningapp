import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pagination } from 'src/app/_models/pagination';
import { PaginationParams } from 'src/app/_models/paginationParams';
import { Post } from 'src/app/_models/post';
import { CoursesService } from 'src/app/_services/courses.service';
import { PostsService } from 'src/app/_services/posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts: Post[];
  pagination: Pagination;
  paginationParams: PaginationParams = new PaginationParams();
  subjectList = [{value: '', display: '<any>'}];

  constructor(private postsService: PostsService, private coursesService: CoursesService) { }

  ngOnInit(): void {
    this.loadPosts();
    this.coursesService.getCourses().subscribe(courses => {
      for (let course of courses) {
        this.subjectList.push({value: course.name, display: course.name});
      }
    });
  }

  loadPosts() {
    this.postsService.getPosts(this.paginationParams).subscribe(response => {
      this.posts = response.result;
      this.pagination = response.pagination;
    })
  }

  pageChanged(event: any) {
    this.paginationParams.pageNumber = event.page;
    this.loadPosts();
  }

  resetFilters() {
    this.paginationParams = new PaginationParams();
    this.loadPosts();
  }

}
