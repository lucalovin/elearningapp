import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Post } from 'src/app/_models/post';
import { PostsService } from 'src/app/_services/posts.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  post: Post;

  constructor(private postsService: PostsService, private route: ActivatedRoute,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadPost();
  }

  loadPost() {
    this.postsService.getPost(+this.route.snapshot.paramMap.get('id')).subscribe(post => {
      this.post = post;
    })
  }

  updatePost() {
    this.postsService.updatePost(this.post).subscribe(() => {
      this.toastr.success('Profile updated successfully');
    });
  }
}
