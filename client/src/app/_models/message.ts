export interface Message {
    id: number;
    senderId: number;
    senderUsername: string;
    senderName: string;
    recipientId: number;
    recipientUsername: string;
    recipientName: string;
    content: string;
    dateRead: Date;
    messageSent: Date;
}