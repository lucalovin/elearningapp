import { Subject } from "./subject";
import { Post } from "./post";
import { Mark } from "./mark";

export interface Member {
    id: number;
    username: string;
    name: string;
    rank: string;
    subjects: Subject[];
    posts: Post[];
    marks: Mark[];
}