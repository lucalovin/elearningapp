export interface MemberDetails {
    id: number;
    username: string;
    name: string;
    rank: string;
}