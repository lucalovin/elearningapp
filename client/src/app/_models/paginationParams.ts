import { Post } from "./post";

export class PaginationParams {
    subjectName: string = "";
    pageNumber = 1;
    pageSize = 5;
}
