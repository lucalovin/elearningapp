export interface Mark {
    id: number;
    value: string;
    subjectName: string;
}