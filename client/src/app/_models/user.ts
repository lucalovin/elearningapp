export interface User {
    username: string;
    name: string;
    token: string;
    roles: string[];
}