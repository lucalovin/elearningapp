export interface SubjectDetails {
    id: number;
    name: string;
}