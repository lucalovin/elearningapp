import { Document } from "./document";
import { MemberDetails } from "./memberDetails";
import { Subject } from "./subject";
import { SubjectDetails } from "./subjectDetails";


export interface Post {
    id: number;
    title: string;
    message: string;
    documents: Document[];
    user: MemberDetails;
    subject: SubjectDetails;
}