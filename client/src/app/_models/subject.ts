import { Member } from "./member";
import { Post } from "./post";

export interface Subject {
    id: number;
    name: string;
    users: Member[];
    posts: Post[];
}